export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  target: 'server',
  head: {
    title: 'payzaty-store',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  loading: {
    color: 'var(--dark-primary)',
    height: '2px'
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['ant-design-vue/dist/antd.css', '~/assets/css/global.css', '~/assets/variables.less'],
  styleResources: {
    scss: [
    ]
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['@/plugins/antd-ui', '@/plugins/service'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    '@nuxtjs/i18n',
    'nuxt-leaflet',
    '@nuxt/http'
  ],
  i18n: {
    /* module options */
    locales: [
      { code: 'en', file: 'en.js', dir: 'ltr', langDir: '~/locales/' },
      { code: 'ar', file: 'ar.js', dir: 'rtl', langDir: '~/locales/' }
    ],
    defaultLocale: 'en',
    vueI18n: {
      messages: {
        en: require('./locales/en.json'),
        ar: require('./locales/ar')
      }
    },
    strategy: 'no_prefix'
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/'
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend (config, ctx) {},
    loaders: {
      less: {
        options: {
          javascriptEnabled: true
        }
      }
    }
  },
  server: {
    host: '0.0.0.0'
  }
}
