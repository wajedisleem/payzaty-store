import Vue from 'vue'

export const state = () => ({
  cart: []
})

export const mutations = {
  SET_ITEM (state, payload) {
    state.cart.push(payload)
  },
  INCREASE_ITEM_QUANTITY (state, payload) {
    state.cart.forEach((item, index) => {
      if (item.id === payload.id) {
        Vue.set(state.cart, index, { ...state.cart[index], quantity: state.cart[index].quantity + 1 })
      }
    })
  },
  DECREASE_ITEM_QUANTITY (state, payload) {
    state.cart.forEach((item, index) => {
      if (item.id === payload.id) {
        if ((item.quantity - 1) === 0) {
          state.cart.splice(index, 1)
        } else {
          Vue.set(state.cart, index, { ...state.cart[index], quantity: state.cart[index].quantity - 1 })
        }
      }
    })
  },
  SET_ITEM_QUANTITY (state, payload) {
    state.cart.forEach((item, index) => {
      if (item.id === payload.id) {
        if (payload.quantity === 0) {
          state.cart = state.cart.filter(item => item.id !== payload.id)
        } else {
          state.cart[index] = { ...state.cart[index], quantity: payload.quantity }
        }
      }
    })
  },
  DELETE_ITEM (state, payload) {
    state.cart.forEach((item, index) => {
      if (item.id === payload.id) { state.cart.splice(index, 1) }
    })
  }

}
export const actions = {

}
