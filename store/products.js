export const state = () => ({
  products: [],
  selectedCategoriesFilter: []
})

export const mutations = {
  SET_PRODUCTS (state, payload) {
    state.products = payload
  },
  SET_CATEGORIES_FILTER (state, payload) {
    state.selectedCategoriesFilter = payload
  }
}
export const actions = {
  async GET_PRODUCTS ({ commit }) {
    const { data } = await this.$getProductsList()
    commit('SET_PRODUCTS', data)
  }
}
